//3.1
// const pointsList = [32, 54, 21, 64, 75, 43];

// let newpointsList = [...pointsList];
// console.log(newpointsList);

//3.2
// const toy = {name: 'Bus laiyiar', date: '20-30-1995', color: 'multicolor'};

// let newToy = {...toy};
// console.log(newToy);

//3.3
// const pointsList = [32, 54, 21, 64, 75, 43];
// const pointsList2 = [54,87,99,65,32];

// let doublePointsList = [...pointsList, ...pointsList2];
// console.log(doublePointsList);

//3.4
// const toy = {name: 'Bus laiyiar', date: '20-30-1995', color: 'multicolor'};
// const toyUpdate = {lights: 'rgb', power: ['Volar like a dragon', 'MoonWalk']};

// let fullToys = {...toy, ...toyUpdate};
// console.log(fullToys);

//3.5
// const colors = ['rojo', 'azul', 'amarillo', 'verde', 'naranja'];

// let [red, blue, yellow, ...allColors] = colors;
// let newColors = [red, blue, ...allColors];
// console.log(newColors);