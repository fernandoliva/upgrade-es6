//5.1
//const ages = [22, 14, 24, 55, 65, 21, 12, 13, 90];

// let newAges = ages.filter((age) => {
//     return age > 18;
// });

// console.log(newAges);

//5.2
// const ages = [22, 14, 24, 55, 65, 21, 12, 13, 90];

// let newAges = ages.filter((age) => {
//     return age % 2 === 0;
// })

// console.log(newAges);

//5.3
// const streamers = [
// 	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
// 	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
// 	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
// 	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
// ];

// const leagueofLegends = streamers.filter(streamer => streamer.gameMorePlayed == 'League of Legends');

// console.log(leagueofLegends);

//5.4
// const streamers = [
// 	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
// 	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
// 	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
// 	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
// ];

// let characterU = streamers.filter((streamer) => {
//     return streamer.name.includes('u');
// })

// console.log(characterU);

//5.5

// const streamers = [
// 	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
// 	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
// 	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
// 	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
// ];

// let newStreamers = streamers.filter((streamer) => {
//     if (streamer.age > 35){
//         return streamer.gameMorePlayed = streamer.gameMorePlayed.toUpperCase();
//     }
// })

// console.log(newStreamers);

//5.6

// const streamers = [
// 	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
// 	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
// 	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
// 	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
// ];

// const $$input = document.querySelector('[data-function="toFilterStreamers"]');

// const search = () => {
//     const input = document.querySelector('[data-function="toFilterStreamers"]');

//     const filter = streamers.filter((streamer) => {
//         if (streamer.name.toLowerCase().includes(input.value.toLowerCase())){
//             return streamer;
//         }
//     })
//     console.log(filter);
// }

// $$input.addEventListener('input', search);

//5.7
// const streamers = [
// 	{name: 'Rubius', age: 32, gameMorePlayed: 'Minecraft'},
// 	{name: 'Ibai', age: 25, gameMorePlayed: 'League of Legends'}, 
// 	{name: 'Reven', age: 43, gameMorePlayed: 'League of Legends'},
// 	{name: 'AuronPlay', age: 33, gameMorePlayed: 'Among Us'}
// ];

// const $$input = document.querySelector('[data-function="toFilterStreamers"]');
// const $$button = document.querySelector('[data-function="toShowFilterStreamers"]');


// const search = () => {
//     const input = document.querySelector('[data-function="toFilterStreamers"]');

//     const filter = streamers.filter((streamer) => {
//         if (streamer.name.toLowerCase().includes(input.value.toLowerCase())){
//             return streamer;
//         }
//     })
//     console.log(filter);
// }

// $$button.addEventListener('click', search);
